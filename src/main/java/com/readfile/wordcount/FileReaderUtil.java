package com.readfile.wordcount;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public   class  FileReaderUtil {
	
	/**
	 * Read file using BufferedReader and read all the strings and add to the list
	 * @param fielPath
	 * @return
	 */
	
	public static List<String>  readFile(String fielPath){
		
		List<String> stringList=new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("C:/Users/Shreekanth/Desktop/domain.xml"))) {
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				if(sCurrentLine.contains("<anchor>")) {
					String[] first = sCurrentLine.split("<anchor>");
					String[] second = first[1].split("</anchor>");
					stringList.add(second[0]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return stringList;
		
		
	}

}
