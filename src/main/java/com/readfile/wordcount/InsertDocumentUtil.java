package com.readfile.wordcount;

import java.net.UnknownHostException;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.util.JSON;

public class InsertDocumentUtil {

	/**
	 * Insert into Mongo DB 
	 * @param MongoDBUrl
	 * @param mostCommon
	 * @param leastCommon
	 */
	
	public void insertDocument (String mongoDBUrl,JSONObject mostCommon,JSONObject leastCommon) {
		
		
		
		
		MongoClient mongoClient;
		try {
			mongoClient = new MongoClient(new MongoClientURI(mongoDBUrl));
			DB database = mongoClient.getDB("Storage");
			DBCollection collection = database.getCollection("count");
			JSONObject json = new JSONObject();
			try {
				json.put("Most Common", mostCommon.toString());
				json.put("Least Common", leastCommon.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			DBObject dbObject = (DBObject) JSON.parse(json.toString());
			collection.insert(dbObject);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	
}
