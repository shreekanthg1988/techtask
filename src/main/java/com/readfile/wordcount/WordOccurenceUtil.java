package com.readfile.wordcount;

import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;

public class WordOccurenceUtil {
	
	
	
    public static void main(String[] args) {
    	
    	 String mongoDBUrl=null;
    	 String fileName=null;
    			 
    	if (args.length > 1) 
        { 
            
    		  fileName=args[0];
            mongoDBUrl=args[1];
          
            
            System.out.println("fileName:"+fileName+"MongoDBUrl:"+mongoDBUrl); 
        } 
        else {
            System.out.println("No command line "+ 
                               "arguments found."); 
        }
     
    	
    	// Using BufferedReader to read file and get the list of words
    	List<String> terms = FileReaderUtil.readFile(fileName);
    	
    	
    	//creates a map of word as key and value as count 
    	ConcurrentMap<String, Integer> wordFreqMap = terms.parallelStream() 
    	.collect(Collectors.toConcurrentMap(w -> w.toLowerCase(), w -> 1, Integer::sum)); // Big O(n)
    	
    	//gets the max value from the map 
    	Integer max = Collections.max(wordFreqMap.values());
    	
    	//gets the least value from the map
    	Integer min=Collections.min(wordFreqMap.values());
    	
    	JSONObject mostCommon = new JSONObject();

    	 JSONObject leastCommon = new JSONObject();
    	
    	 for(Entry<String, Integer> entry : wordFreqMap.entrySet()) {
    		 try {
    	       // Integer value = entry.getValue();
    	        if(null != entry.getValue() && max == entry.getValue()) {
    	        	
    					mostCommon.put((String) entry.getKey(), (int) entry.getValue());
    			
    	        }
    	        
    	        if(null != entry.getValue() && min == entry.getValue()) {
    	        	
    	        	leastCommon.put((String) entry.getKey(), (int) entry.getValue());
    		
    	        }
    			} catch (JSONException e) {
    				System.out.println("Error ocuured "+e.getMessage());
    				e.printStackTrace();
    			};
    	    }
    	
    	
    	
    	
    	   
    	
    	System.out.println("Most Common Ocurrences="+mostCommon);
    	
    	System.out.println("Least common ocurrences"+leastCommon);
		
        
       
		
		InsertDocumentUtil insertDocumentUtil=new InsertDocumentUtil();
		
		insertDocumentUtil.insertDocument(mongoDBUrl, mostCommon, leastCommon);
		
		System.out.println("successfully inserted into DB");
    }
    
 
    
}